const express = require('express');
const movies = require('./routes/movies.js');
const people = require('./routes/people.js');
const customers = require('./routes/customers');
const genres = require('./routes/genres');
const rentals = require('./routes/rentals');
const users = require('./routes/users');
const auth = require('./routes/auth').router;
const helmet = require('helmet');
const compression = require('compression');

const port = 3000;
const app = express();
if(process.env.NODE_ENV === 'development')
require('dotenv').config({ path: './config/settings.env' });

const mongoose = require('mongoose');
mongoose.connect(process.env.db, {useNewUrlParser: true, useFindAndModify: false,  useCreateIndex: true})
    .then(() => {console.log('Connected to MongoDB')}).catch(err => {console.log('Could not connect to Mongodb', err)});

app.use(express.json());

app.use('/api/movies', movies);
app.use('/api/people', people);
app.use('/api/customers', customers);
app.use('/api/genres', genres);
app.use('/api/rentals', rentals);
app.use('/api/users', users);
app.use('/api/auth', auth);
app.use(compression());
app.use(helmet());

const server = app.listen(process.env.PORT||port, () => {
    console.log(`Express running → PORT ${server.address().port}`);
});


module.exports ={
    server,
    mongoose
} ;



