const validate = require('../../routes/auth').validate;

let user;
describe('Auth Joi validation', function() {
    beforeEach(function() {
        user = {
           email: "Barlerer@gmail.com",
            password: "12345"
       }
    });
   it('should return error email cannot be empty', function() {
       user.email="";
       const {error}  = validate(user);
       expect(error).toBeDefined();
       expect(error.details[0].message).toMatch(/empty/) ;
   });

    it('should return error no email', function() {
        user = {
            password: user.password
        };
        const {error}  = validate(user);
        expect(error).toBeDefined();
        expect(error.details[0].message).toMatch(/required/) ;
    })

    it('should return error not an email', function() {
        user.email = "abc";
        const {error}  = validate(user);
        expect(error).toBeDefined();
        expect(error.details[0].message).toMatch(/valid/) ;
    })

    it('should return error not an email', function() {
        user.password = "a";
        const {error}  = validate(user);
        expect(error).toBeDefined();
        expect(error.details[0].message).toMatch(/password/) ;
    })

    it('should not return an error', function() {
        const {error}  = validate(user);
        expect(error).toBeNull();
    })
});