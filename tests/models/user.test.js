const {User, validateUser} = require("../../models/user");

let user ={
    email : "Barlerer@gmail.com",
    password : "123456"
};


describe("Test user validate", () => {
    beforeEach(() => {
         user ={
            email : "Barlerer@gmail.com",
            password : "123456"
        };
    });
    test("should throw password too short", () => {
        user.password = "12";
        const {error} = validateUser(user);
        expect(error.details[0].message).toMatch(/least/);
        expect(error.details[0].context.key).toBe('password');
    });

    test("should throw email not valid", () => {
        user.email = "ac";
        const {error} = validateUser(user);
        expect(error.details[0].message).toMatch(/valid/);
        expect(error.details[0].context.key).toBe('email');
    });

    test("should validate", () => {
        const {error} = validateUser(user);
        expect(error).toBeNull();
    })
});

