const {User} = require('../../../models/user');
const request = require('supertest');
const {mongoose} = require('../../../app');
let server;
describe('testing POST for users', function() {
   beforeEach(function()  {
      server = require('../../../app').server;
   });
   afterEach(async function() {
      server.close();
      await User.deleteMany({});
      mongoose.connection.close()
   });

   it('should create a user',async function() {
      let user = {
         email: "testing123@gmail.com",
         password: "Yoyoyoy"
      };
      const result = await request(server).post('/api/users').send(user);
      expect(result.status).toBe(200);
      expect(result.body).toHaveProperty("email", "testing123@gmail.com");
      expect(result.body).toHaveProperty("isAdmin", false);

   }) ;
});
