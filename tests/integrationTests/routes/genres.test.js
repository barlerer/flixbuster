const {Genre} = require('../../../models/genre');
const {mongoose} = require('../../../app');
const request = require('supertest');

let server;
describe('testing GET for genres', function() {
    beforeEach(function()  {
        server = require('../../../app').server;
    });
    afterEach(async function() {
        server.close();
        await Genre.deleteMany({});
        mongoose.connection.close()
    });

    it('should create a genre',async function() {
       let genre = {
           name: "abcde"
       };
        const result = await request(server).post('/api/genres').send(genre);
        expect(result).toBeDefined();
    }) ;
});
