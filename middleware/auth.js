const jwt = require('jsonwebtoken');

module.exports = function (req, res ,next) {
  const token = req.header('x-auth-token');
    if (!token) {
        return res.status(401).send('No token provided')
    }

    try{
        const user = jwt.verify(token, process.env.JWTPASSKEY);
        req.user = user;
        next();
    } catch (ex) {
        return res.status(400).send('Bad token');
    }
};