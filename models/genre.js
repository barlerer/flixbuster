const mongoose = require('mongoose');
const Joi = require('@hapi/joi');
const schema = mongoose.Schema({
    name: {
        type: String,
        required: true
    }
});

const Genre = mongoose.model('Genre', schema);


function validateGenre(genre) {
    let joiSchema = {
        name: Joi.string().min(3).max(270)
    };
    return Joi.validate(genre, joiSchema);
}

module.exports.Genre = Genre;
module.exports.genreSchema = schema;
module.exports.validate = validateGenre;