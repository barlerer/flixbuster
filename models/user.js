const mongoose = require('mongoose');
const Joi = require('@hapi/joi');
const jwt = require('jsonwebtoken');

const schema = mongoose.Schema({
   email: {
       type: String,
      required : true,
      unique : true,
      min: 5,
      max: 256
   } ,
    password: {
        required: true,
        type: String,
        min: 4,
        max: 256
    },
    isAdmin:{
       type: Boolean,
        default: false
    }
});
schema.methods.generateToken = function() {
    return jwt.sign({email: this.email, isAdmin: this.isAdmin}, process.env.JWTPASSKEY);
};
const User = mongoose.model("user", schema);

function validateUser(user) {
    const joiSchema = {
        email: Joi.string().email({ minDomainSegments: 2 }).required(),
        password: Joi.string().min(3).max(256).required(),
        isAdmin: Joi.boolean()
    };
    return Joi.validate(user, joiSchema);
}

module.exports = {
    User,
    validateUser
};