const Joi = require('@hapi/joi');
const mongoose = require('mongoose');

const schema = mongoose.Schema({
    name: {
        required: true,
        type: String,
        minlength: 3,
        maxlength: 50
    },
    isGold: {
        type: Boolean,
        default: false
    },
    phone: {
        required: true,
        type: String,
        minlength: 3,
        maxlength: 50
    },
    Created :{
        type: Date,
        default : Date.now
    }
});

const Customer = mongoose.model('Customer', schema);

function validateCustomer(cus) {
    const schema = {
        name: Joi.string().min(5).max(50).required(),
        phone: Joi.string().min(5).max(50).required(),
        isGold: Joi.boolean()
    };
    return Joi.validate(cus, schema);
}

module.exports.Customer = Customer;
module.exports.validate = validateCustomer;
exports.schema = schema;