const mongoose = require('mongoose');
const {genreSchema} = require('../models/genre');
const Joi = require('@hapi/joi');
const schema = mongoose.Schema({
    id: Number,
    name: String,
    year: Number,
    actors: [String],
    director: [String],
    genre: {
        type: genreSchema,
        required: true
    },
    stock:{
        type: Number,
        min:0,
        max: 257,
        default: 0
    },
    dailyRentalRate: {
        type: Number,
        min: 0,
        max: 255
    },
    Created: {
        type: Date,
        default: Date.now
    }
});

const Movies = mongoose.model('Movies', schema);

function validateMovie(movie) {
    const schema = {
        name: Joi.string().min(3).max(250).required(),
        director: Joi.array().min(1),
        year: Joi.number().min(1500).max(3000),
        genreId: Joi.string().min(3),
        actors: Joi.array(),
        id: Joi.number()
    };
    return Joi.validate(movie, schema);
}
//module.exports.Movies = Movies;
module.exports = {
    Movies,
    validateMovie
};