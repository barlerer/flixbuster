const mongoose = require('mongoose');

const schema = new mongoose.Schema({
    customer: {
        type: new mongoose.Schema({
            customerId: {
                type: String
            },
            name: {
                type: String,
                required: true
            },
            isGold: {
                type: Boolean,
                default: false
            }
        }),
        required: true
    },
    movie: {
        type: new mongoose.Schema({
            movieId: {
                type: String
            },
            name: {
                type:String,
                required:true
            }
        }),
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
});
const Rental = mongoose.model('rental', schema);
module.exports = {
    Rental
};
