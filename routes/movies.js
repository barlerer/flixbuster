const express = require('express');
const {Movies, validateMovie} = require('../models/movie');
const {Genre} = require('../models/genre');
const router = express.Router();

router.post('/', async (req, res) => {
    const {error} = validateMovie(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message)
    }
    const genre = await Genre.findById(req.body.genreId);
    if (!genre) return  res.status(400).send("Bad genre ID");
    const movie = new Movies({
        name: req.body.name,
        id: parseInt(req.body.id),
        year: parseInt(req.body.year),
        actors: req.body.actors,
        director: req.body.director,
        dailyRentalRate:req.body.dailyRentalRate,
        genre: {
            _id: genre._id,
            name: genre.name
        }
    });
    const newMovie = await movie.save();
    res.send(newMovie);
});

//This deletes the movie with a specific id.
router.delete('/:id',async (req, res) => {
    let movie = await Movies.findByIdAndRemove({_id:req.params.id});
    res.send(movie)
});

router.get('/', async (req, res) => {
    const result = await Movies.find().sort({name: 1});
    res.send(result)
});

router.get('/:id', async function (req, res) {
    const movie = await findMovieById(req.params.id);
    res.send(movie);
});

router.put('/:id', async function (req, res) {
    const {error} =validateMovie(req.body);
    if(error) {
        return res.send(error.details[0].message);
    }
    const newMovie = req.body;
    let movieToReplace = await Movies.findByIdAndUpdate(req.params.id,
        newMovie
    , {new: true});
    return res.send(movieToReplace)
});

async function findMovieById(movieId) {
    const movies = await Movies.findOne({_id: movieId});
    return movies;
}

async function findMovieByDirector(directorName) {
    const movies = await Movies.find({Director: directorName});
    return movies[0];
}

async function deleteMovie(id) {
    const result = await Movies.deleteOne({_id: id});
}

module.exports = router;