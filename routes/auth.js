const Joi = require('@hapi/joi');
const {User} = require('../models/user');
const bycrpt = require('bcrypt');
const router = require('express').Router();

router.post('/', async function(req, res ,next) {
    let userInput = req.body;
   const {error} = validate(userInput);
    if (error) {
       return  res.status(400).send(error.details[0].message)
    }
    const user = await User.findOne({email: userInput.email});
    if(!user) {
        return res.status(400).send('Invalid email/password combination')
    }
    const passwordAuth = await bycrpt.compare(userInput.password, user.password);
    if(!passwordAuth) {
        return res.status(400).send('Invalid email/password combination')
    }

    return res.set('x-auth-token', user.generateToken()).send('Login success');
});

function validate(auth) {
    const schema = {
        email: Joi.string().email().required(),
        password: Joi.string().min(2).required()
    };
    return Joi.validate(auth, schema)
}

module.exports = {
    router,
    validate
};