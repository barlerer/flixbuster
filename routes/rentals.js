const {Rental} = require('../models/rental');
const {Customer} = require('../models/customer');
const {Movies} = require('../models/movie');
const mongoose = require('mongoose');

const router = require('express').Router();
router.post('/', async (req, res) => {
    let customer = req.body.customer;
    if(!mongoose.Types.ObjectId.isValid(customer.customerId)) {
        return res.status(404).send("Customer ID is invalid")
    }
    let customerDb = await Customer.findById(customer.customerId);
    if(!customerDb) {
        return res.status(404).send("Customer not found")
    }
    let movie = req.body.movie;
    if(!mongoose.Types.ObjectId.isValid(movie.movieId)) {
        return res.status(404).send("Movie ID is invalid")
    }
    let movieDb = await Movies.findById(movie.movieId);
    if(!movieDb) {
        return res.status(404).send("Movie not found")
    }
    const rental = new Rental({
        customer:{
            _id: customerDb._id,
            isGold:customerDb.isGold,
            name: customerDb.name
        },
        movie:{
            _id:movieDb._id,
            name:movieDb.name
        }
    });
    let result = await rental.save();
    res.send(result);
});

module.exports = router;