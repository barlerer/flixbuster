const express = require('express');
const router = express.Router();
const {Customer, validate} = require('../models/customer');

router.get('/', async (req, res) => {
    const result = await Customer.find().sort({name : 1});
    res.send(result)
});

router.post('/', async (req, res) => {
    const {error} = validate(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message)
    }

    let cus = new Customer({
        name: req.body.name,
        phone: req.body.phone,
        isGold: req.body.isGold
    });
    cus = await cus.save();
    res.send(cus)
});

module.exports = router;