const {Genre, validate} = require('../models/genre');
const router = require('express').Router();
const auth = require('../middleware/auth');
const admin = require('../middleware/admin');

router.get('/', async function(req, res) {
    const genres = await Genre.find({});
    res.send(genres);
});

router.get('/:id', async (req, res) => {
   const result = await Genre.findById(req.params.id);
   res.send(result);
});

router.delete('/:id',[auth, admin], async (req, res) =>{
    const result = await Genre.findByIdAndDelete(req.params.id);
    if(!result) {
        return res.status(404).send("No genre with this ID")
    }
    res.send(result);
});

router.post('/', async (req, res) => {
    const {error} = validate(req.body);
    if(error) {return  res.status(400).send(error.details[0].message);}
    let genre = new Genre({
        name: req.body.name
    });
    genre =await genre.save();
    res.send(genre)
        }
);

module.exports = router;
