const {User, validateUser} = require('../models/user');
const bcrypt = require('bcrypt');
const router = require('express').Router();
const auth = require('../middleware/auth');

router.get('/me', auth, async function (req, res, next) {
    const user = await User.findOne({email: req.user.email});
    const result = {
        email: user.email
    };
    return res.send(result);
});

router.post('/',async (req, res) => {
   const {error} = validateUser(req.body);
   if(error) {
       return res.status(400).send(error.details[0].message);
   }
   let user = new User({
       email: req.body.email,
       password: await bcrypt.hash(req.body.password, 8),
       isAdmin: req.body.isAdmin
   });
    user = await user.save();
    let token = user.generateToken();

    user = {
        email: user.email,
        isAdmin: user.isAdmin,
        _id: user._id
    };
    res.header('x-auth-token', token).send(user);
});

module.exports = router;