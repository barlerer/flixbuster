const express = require('express');
const router = express.Router();
const people = [{name: 'Bar Lerer', id:1},
    {name: 'Ayala Hassan', id:2}];

function findPeopleById(id) {
   return people.find( (person) => {
        if (person.id === id) {
            return person;
        }
    })
}

router.get('/', (req, res) => {
   res.send(people);
});

router.get('/:id', (req, res) => {
    const personId = parseInt(req.params.id);
    const person = findPeopleById(personId);
    res.send(person);
});

module.exports = router;

